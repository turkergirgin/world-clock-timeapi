import 'package:flutter/material.dart';
import 'package:world_clock_timeapi/test.dart';

//import 'home_page.dart';

void main() {
  runApp(const WorldClockTimeAPI());
}

class WorldClockTimeAPI extends StatelessWidget {
  const WorldClockTimeAPI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'World Clock TimeAPI',
      theme: ThemeData(primarySwatch: Colors.lightBlue),
      //home: const HomePage(),
      home: TestIt(),
    );
  }
}
