import 'package:dart_ipify/dart_ipify.dart';

Future<String> getIP() async {
  try {
    final ipv4 = await Ipify.ipv4();
    return ipv4;
  } catch (e) {
    return e.toString();
  }
}
