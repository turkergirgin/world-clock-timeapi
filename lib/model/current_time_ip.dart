// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

Welcome welcomeFromJson(String str) => Welcome.fromJson(json.decode(str));

String welcomeToJson(Welcome data) => json.encode(data.toJson());

class Welcome {
    Welcome({
        required this.month,
        required this.day,
        required this.year,
        required this.hour,
        required this.minute,
        required this.seconds,
        required this.milliSeconds,
        required this.dateTime,
        required this.date,
        required this.time,
        required this.timeZone,
        required this.dayOfWeek,
        required this.dstActive,
    });

    final int year;
    final int month;
    final int day;
    final int hour;
    final int minute;
    final int seconds;
    final int milliSeconds;
    final DateTime dateTime;
    final String date;
    final String time;
    final String timeZone;
    final String dayOfWeek;
    final bool dstActive;

    factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
        year: json["year"],
        month: json["month"],
        day: json["day"],
        hour: json["hour"],
        minute: json["minute"],
        seconds: json["seconds"],
        milliSeconds: json["milliSeconds"],
        dateTime: DateTime.parse(json["dateTime"]),
        date: json["date"],
        time: json["time"],
        timeZone: json["timeZone"],
        dayOfWeek: json["dayOfWeek"],
        dstActive: json["dstActive"],
    );

    Map<String, dynamic> toJson() => {
        "year": year,
        "month": month,
        "day": day,
        "hour": hour,
        "minute": minute,
        "seconds": seconds,
        "milliSeconds": milliSeconds,
        "dateTime": dateTime.toIso8601String(),
        "date": date,
        "time": time,
        "timeZone": timeZone,
        "dayOfWeek": dayOfWeek,
        "dstActive": dstActive,
    };
}
