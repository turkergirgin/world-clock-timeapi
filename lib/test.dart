import 'package:flutter/material.dart';
import 'package:world_clock_timeapi/get_ip.dart';

class TestIt extends StatefulWidget {
  TestIt({Key? key}) : super(key: key);

  @override
  State<TestIt> createState() => _TestItState();
}

class _TestItState extends State<TestIt> {
/*   Future<String> _getIPV4() async {
    try {
      final ipv4 = await Ipify.ipv4();
      debugPrint(ipv4.toString());
      return ipv4;
    } catch (e) {
      debugPrint(e.toString());
      return e.toString();
    }
  } */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('World Clock TimeAPI Test Page'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
            child: Text("Device Clock:" +
                "\n" +
                DateTime.now().hour.toString() +
                ":" +
                DateTime.now().minute.toString() +
                ":" +
                DateTime.now().second.toString()),
          ),
          Center(
            child: FutureBuilder<String>(
              future: getIP(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  //karakter sayısı 16 dan az ise IP bilgisi geğilse hata mesajı
                  if (snapshot.data!.length < 16) {
                    return Text(snapshot.data.toString());
                  } else {
                    return const Text(
                        "Error: Cannot get IP information, please check your internet connection!");
                  }
                } else {
                  return const CircularProgressIndicator();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
